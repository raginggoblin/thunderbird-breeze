# thunderbird-breeze

KDE Breeze theme for Thunderbird email client

This is a set of userChrome.css files to change the look of Thunderbird, inspired by https://github.com/BDeliers/thunderbird-monterail. 
It is not a complete theme but just a few icons from https://github.com/KDE/breeze-icons. Thunderbird sticks out on KDE like a sore thumb. 
At least the folder pane looks a bit better using the breeze icons.

#### Before   
<img src="https://gitlab.com/raginggoblin/thunderbird-breeze/raw/master/screenshots/Default.png" />

#### After
<img src="https://gitlab.com/raginggoblin/thunderbird-breeze/raw/master/screenshots/BreezeIcons.png" />

## Howto apply
If you have Git installed you can execute the following command from your Thunderbird profile folder:

 ```<user>@<hostname>:~/.thunderbird/<profile>.default git clone git@gitlab.com:raginggoblin/thunderbird-breeze.git chrome```

This results in a ```chrome``` folder with the content of this repository.

If you do not have Git installed you can download the archive from: https://gitlab.com/raginggoblin/thunderbird-breeze/-/archive/master/thunderbird-breeze-master.zip

Exctract the content and rename the folder to ```chrome``` and put it in your Thunderbird profile folder.

Restart Thunderbird afterwards.


 
  